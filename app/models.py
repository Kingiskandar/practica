from operator import mod
from django.db import models

class Persona (models.Model): #nombre de la clase que creamos
    numero=models.BigAutoField(primary_key=True) #atributos de la clase que creamos
    apellido=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    def __str__(self): #comando para mostrar los datos guardados
        texto='{} {}'.format( #guarda todas las variables de la clase persona
            self.apellido,
            self.nombre)

        return texto
    

# Create your models here.
